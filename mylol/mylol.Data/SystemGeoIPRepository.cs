﻿using mylol.Helper;
using mylol.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Data
{
    public class SystemGeoIPRepository
    {
        
        public static SystemGeoIP GetLatitudeAndLongitudeOfTheCityOrCountry(string cityName="", long provinceID=0, long countryID=0)
        {
            SystemGeoIP result = new SystemGeoIP();
            result.latitude = String.Empty;
            result.longitude = String.Empty;

            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {
                string sql = @" select top 1 latitude,longitude from system_geoip where city_name = @cityName collate SQL_Latin1_General_CP1253_CI_AI and reg_name like (select p_name collate SQL_Latin1_General_CP1253_CI_AI from site_provinces where p_id=@provinceID) ";

                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@cityName", cityName);
                cmd.Parameters.AddWithValue("@provinceID", provinceID);
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    if (reader.Read())
                    {
                        if (reader["latitude"] != null)
                            result.latitude = reader["latitude"].ToString();
                        if (reader["longitude"] != null)
                            result.longitude = reader["longitude"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception Error Message: " + ex.Message);
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;
                    if (connection.State != System.Data.ConnectionState.Closed)
                        connection.Close();
                }



                /* second sql statement start here */
                if (result.latitude.Equals(String.Empty) && result.longitude.Equals(String.Empty))
                {
                    sql = @" select top 1 latitude,longitude from system_geoip where co_name   like (select co_name collate SQL_Latin1_General_CP1253_CI_AI from site_countries where co_id=@countryID) ";
                    cmd = new SqlCommand(sql, connection);
                    cmd.Parameters.AddWithValue("@countryID", countryID);                    
                    cmd.CommandType = CommandType.Text;

                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    reader = cmd.ExecuteReader();
                    try
                    {
                        if (reader.Read())
                        {
                            if (reader["latitude"] != null)
                                result.latitude = reader["latitude"].ToString();
                            if (reader["longitude"] != null)
                                result.longitude = reader["longitude"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("Exception Error Message: " + ex.Message);
                    }
                    finally
                    {
                        reader.Close();
                        reader.Dispose();
                        reader = null;
                        if (connection.State != System.Data.ConnectionState.Closed)
                            connection.Close();
                    }
                }
                /* second sql statement end here */


                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

    }
}
