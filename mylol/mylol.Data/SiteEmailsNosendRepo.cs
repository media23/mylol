﻿using mylol.Helper;
using mylol.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Data
{
    public class SiteEmailsNosendRepo
    {

        public static int RemoveEmailFromSpamList(string emailAddress)
        {
            int result = 0;

            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {
                string sql = @" DELETE  from site_emails_nosend where sed_email=@emailAddress ";
                

                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@emailAddress", emailAddress);
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                cmd.ExecuteNonQuery();

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

    }
}
