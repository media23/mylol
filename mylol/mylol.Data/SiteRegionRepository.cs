﻿using mylol.Helper;
using mylol.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Data
{
    public class SiteRegionRepository
    {

        public static List<SiteRegion> GetAreaByCityID(long cityID)
        {
            List<SiteRegion> result = new List<SiteRegion>();

            string sql = @" SELECT r_id, r_name FROM site_cities,site_regions where r_p_id = c_p_id and c_id = " + cityID.ToString() + "order by r_name asc ";
            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);                
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        SiteRegion siteRegion = new SiteRegion();
                        siteRegion.r_id = Convert.ToInt64(reader["r_id"].ToString());
                        siteRegion.r_name = reader["r_name"].ToString();
                        result.Add(siteRegion);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception Error Message: " + ex.Message);
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;
                    if (connection.State != System.Data.ConnectionState.Closed)
                        connection.Close();
                }

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

        public static SiteRegion GetProvinceIDAndCountryIDByRegionID(long regionID)
        {
            SiteRegion result = new SiteRegion();

            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {
                string sql = @" Select r_co_id, r_p_id from site_regions where r_id =" + regionID;

                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    if (reader.Read())
                    {
                        if (Convert.ToInt64(reader["r_co_id"].ToString()).Equals(3))
                            result.pro_real_reg_id = 997; //pro_reg2 = 997
                        else if (Convert.ToInt64(reader["r_co_id"].ToString()).Equals(2))
                            result.pro_real_reg_id = 998; //pro_reg2 = 998
                        else if (Convert.ToInt64(reader["r_co_id"].ToString()).Equals(1))
                        {
                            if (Convert.ToInt64(reader["r_p_id"].ToString()).Equals(1))
                                result.pro_real_reg_id = regionID; //pro_reg2 = pro_reg
                            else
                                result.pro_real_reg_id = 999; //pro_reg2 = 999
                        }

                        result.pro_co_id = Convert.ToInt64(reader["r_co_id"].ToString()); // pro_co_id = rsReg.fields("r_co_id")
                        result.pro_p_id = Convert.ToInt64(reader["r_p_id"].ToString()); // pro_p_id = rsReg.fields("r_p_id")
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception Error Message: " + ex.Message);
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;
                    if (connection.State != System.Data.ConnectionState.Closed)
                        connection.Close();
                }

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

        public static bool IsTooManyAccountCreatedFromSameIP(string ipAddress)
        {
            bool result = false;

            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {
                string sql = @" select count(pro_id)x from profiles where pro_ip like '" + ipAddress + "' and pro_date > dateadd(day,-1,getdate()) ";
                
                SqlCommand cmd = new SqlCommand(sql, connection);                
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    if (reader.Read())
                        result = Convert.ToInt32(reader["x"].ToString()) > 5;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception Error Message: " + ex.Message);
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;
                    if (connection.State != System.Data.ConnectionState.Closed)
                        connection.Close();
                }

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

    }
}
