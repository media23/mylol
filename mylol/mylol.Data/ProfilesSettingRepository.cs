﻿using mylol.Helper;
using mylol.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Data
{
    public class ProfilesSettingRepository
    {

        public static int AddUser(ProfilesSetting profilesSetting, string sqlShoutFilter)
        {
            int result = 0;

            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {
                string sql = @" INSERT INTO profiles_settings ( ps_credits,  ps_global_msg_id,  ps_level, ps_level_date,  ps_pro_id,  ps_lang,  ps_activated,  ps_ip,                                                                      ps_timezone, ps_chat, ps_privacy, ps_msg_nbr,     ps_shout_fr_age,ps_shout_to_age ) 
                                                       VALUES (@ps_credits, @ps_global_msg_id, @ps_level,     GETDATE(), @ps_pro_id, @ps_lang, @ps_activated, @ps_ip, (SELECT ISNULL(r_timezone,-5) from site_regions WITH(NOLOCK) where r_id = @r_id),       3,          1,          0, " + sqlShoutFilter +              " ) ";
                
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@ps_credits", profilesSetting.ps_credits);
                cmd.Parameters.AddWithValue("@ps_global_msg_id", profilesSetting.ps_global_msg_id);
                cmd.Parameters.AddWithValue("@ps_level", 10); // newLevel = 10
                cmd.Parameters.AddWithValue("@ps_pro_id", profilesSetting.ps_pro_id);
                cmd.Parameters.AddWithValue("@ps_lang", profilesSetting.ps_lang);
                cmd.Parameters.AddWithValue("@ps_activated", 1); // 1
                cmd.Parameters.AddWithValue("@ps_ip", profilesSetting.ps_ip);
                cmd.Parameters.AddWithValue("@r_id", profilesSetting.r_id);                
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                cmd.ExecuteNonQuery();

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

        public static LostPassword LostPassword(string loginOrEmail, int us_lang = 0) // lost password
        {
            LostPassword result = new LostPassword();
            result.message = String.Empty;
            result.pro_id = 0;
            result.ps_lang = 0;
            result.pro_login = String.Empty;

            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {
                string sql = @" SELECT pro_id,ps_lang,pro_login FROM profiles_settings WITH(nolock) INNER JOIN Profiles WITH(nolock) ON ps_pro_id = pro_id ";
                sql += " where ((pro_realemail = '" + loginOrEmail + "') OR ((pro_login='" + loginOrEmail + "' AND pro_login_mylol is null) OR (pro_login_mylol='" + loginOrEmail + "' AND pro_login_mylol is not null))) ";

                SqlCommand cmd = new SqlCommand(sql, connection);                
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    if (reader.Read())
                    {
                        result.pro_id = Convert.ToInt64(reader["pro_id"].ToString());
                        result.ps_lang = reader["ps_lang"] == null ? us_lang : Convert.ToInt64(reader["ps_lang"].ToString());

                        sql = @"SELECT count(se_id) AS Count FROM site_emails WHERE se_sme_id = 91 AND se_date_send BETWEEN '" + DateTime.Now.Date.ToString() + "' AND '" + DateTime.Now.Date.AddDays(1).ToString() + "' AND se_to_pro_id=" + result.pro_id.ToString();
                        cmd = new SqlCommand(sql, connection);
                        cmd.CommandType = CommandType.Text;
                        if (connection.State != ConnectionState.Open)
                            connection.Open();
                        SqlDataReader readerTwo = cmd.ExecuteReader();
                        try {
                            if (Convert.ToInt64(readerTwo["Count"].ToString()) >= 5)
                                result.message = "You have reached the limit of 5 password requests.";
                            else
                            {
                                result.message = "An email with your login informations has been sent to you! <b>NOTE : This email can be placed in your spam folder in your email account.</b>";
                                result.pro_login = reader["pro_login"].ToString();

                                //db_master.execute("INSERT INTO site_emails (se_fr_pro_id,se_to_pro_id,se_sme_id,se_langue_id) values(-1," & rs("pro_id") & ","&IIF(session_web_id=0,"91","71")&"," & varLang & ")")

                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine("Exception Error Message: " + ex.Message);
                        }
                        finally
                        {
                            readerTwo.Close();
                            readerTwo.Dispose();
                            readerTwo = null;
                            if (connection.State != System.Data.ConnectionState.Closed)
                                connection.Close();
                        }

                    }
                    else
                        result.message = "Error, the member\'s login/email wasn\'t found.";
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception Error Message: " + ex.Message);
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;
                    if (connection.State != System.Data.ConnectionState.Closed)
                        connection.Close();
                }

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

    }
}
