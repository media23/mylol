﻿using mylol.Helper;
using mylol.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Data
{
    public class ProfileRepository
    {
        
        public static int RegisterProfile(Profile profile)
        {
            int result = 0;

            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {
                string sql = @" 
                                INSERT INTO Profiles( pro_reg_id,  pro_regions,  pro_top5,  pro_login,  pro_dob,  pro_age,  pro_sex,  pro_ville,  pro_c_id,  pro_realemail,  pro_pic_url,  pro_date,  pro_show,  pro_visit,  pro_vo_show,  pro_chat_show,  pro_sendemail,  pro_orisexe,  pro_fb_id,  pro_twitter_id,  pro_mbr_bar_gender,  pro_level,  pro_co_id,  pro_p_id,  pro_web_id,  pro_statut,  pro_ip,                                               pro_geo,  pro_latitude,  pro_longitude,                                                                                              pro_secure,  pro_classeur )
                                              VALUES(@pro_reg_id, @pro_regions, @pro_top5, @pro_login, @pro_dob, @pro_age, @pro_sex, @pro_ville, @pro_c_id, @pro_realemail,         NULL, getdate(),        21,          0,            1,              1,              0,            3, @pro_fb_id, @pro_twitter_id, @pro_mbr_bar_gender, @pro_level, @pro_co_id, @pro_p_id, @pro_web_id,           1, @pro_ip, geography::Point(@pro_latitude, @pro_longitude, 4326), @pro_latitude, @pro_longitude, (lower(CONVERT(NVARCHAR(32),HashBytes('MD5',lower('@password_'+(CONVERT(VARCHAR, pro_date, 120)))),2))), @pro_classeur ) 
                                SELECT SCOPE_IDENTITY() AS pro_id
                              ";

                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@pro_reg_id", profile.pro_reg_id);
                cmd.Parameters.AddWithValue("@pro_regions", profile.pro_regions);
                cmd.Parameters.AddWithValue("@pro_top5", 0);
                cmd.Parameters.AddWithValue("@pro_login", profile.pro_login);
                cmd.Parameters.AddWithValue("@pro_dob", profile.pro_dob);
                cmd.Parameters.AddWithValue("@pro_age", profile.pro_age);
                cmd.Parameters.AddWithValue("@pro_sex", profile.pro_sex);
                cmd.Parameters.AddWithValue("@pro_ville", profile.pro_ville);
                cmd.Parameters.AddWithValue("@pro_c_id", profile.pro_c_id);
                cmd.Parameters.AddWithValue("@pro_realemail", profile.pro_realemail);
                cmd.Parameters.AddWithValue("@pro_fb_id", profile.pro_fb_id);
                cmd.Parameters.AddWithValue("@pro_twitter_id", profile.pro_twitter_id);
                cmd.Parameters.AddWithValue("@pro_mbr_bar_gender", profile.pro_mbr_bar_gender);
                cmd.Parameters.AddWithValue("@pro_level", profile.pro_level);
                cmd.Parameters.AddWithValue("@pro_co_id", profile.pro_co_id);
                cmd.Parameters.AddWithValue("@pro_p_id", profile.pro_p_id);
                cmd.Parameters.AddWithValue("@pro_web_id", profile.pro_web_id);
                cmd.Parameters.AddWithValue("@pro_ip", profile.pro_ip);
                cmd.Parameters.AddWithValue("@pro_latitude", profile.pro_latitude);
                cmd.Parameters.AddWithValue("@pro_longitude", profile.pro_longitude);                
                cmd.Parameters.AddWithValue("@password", profile.pro_pass);                
                cmd.Parameters.AddWithValue("@pro_classeur", ((profile.pro_age - 4).ToString() + "," + (profile.pro_age + 4).ToString() + ",0,0,0"));
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    if (reader.Read())
                        result = Convert.ToInt32(reader["pro_id"].ToString());
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    connection.Close();
                }

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

        public static bool IsEmailAddressExist(string emailAddress)
        {
            bool result = false;

            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {
                string sql = @" SELECT COUNT(*) AS Count FROM profiles WHERE pro_realemail = @EmailAddress AND pro_show IN (1,4,21) ";
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@EmailAddress", emailAddress);
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    if (reader.Read())
                        result = Convert.ToInt32(reader["Count"].ToString()) > 0;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception Error Message: " + ex.Message);
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;
                    if (connection.State != System.Data.ConnectionState.Closed)
                        connection.Close();
                }

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

        public static bool IsUsernameExist(string username)
        {
            bool result = false;

            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {
                string sql = @" SELECT COUNT(*) AS Count FROM profiles WHERE lower(pro_login) = @Username ";                
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@Username", username.ToLower());
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    if (reader.Read())
                        result = Convert.ToInt32(reader["Count"].ToString()) > 0;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception Error Message: " + ex.Message);
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;
                    if (connection.State != System.Data.ConnectionState.Closed)
                        connection.Close();
                }

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

    }
}
