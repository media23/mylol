﻿using mylol.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Data
{
    public class SiteCityRepository
    {
        // search cities by town, result JSON string
        public static string SearchCities(string town, int session_web_id = 0)
        {
            string result = String.Empty;

            string varTop = String.Empty;
            if (town.Trim() != String.Empty)
                varTop = " top 45 ";

            string sql = @" select top 45 c_id,c_name,(select r_name from site_regions where c_reg_id=r_id)c_p,c_co_id,(c_co_name)c_co from site_cities where CONTAINS(c_name, '""*@town*""') and c_p_id=1 ";
            if (session_web_id.Equals(1))
            {
                sql = @" select * from (SELECT " + varTop + " c_id, c_name, (c_p_name)c_p ,c_co_id, (c_co_name)c_co,c_nbr_used from site_cities ";
                if (town.Trim() != String.Empty)
                    sql += @" where CONTAINS(c_name, '""*@town*""')  order by (case c_co_id when 2 then 0 else 1 end) asc ";
                sql += @")r order by c_nbr_used desc ";
            }

            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {                
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@town", town);
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    long ccoid = 0, cccio = 0;
                    result = @"{""city_list"":[";
                    while (reader.Read())
                    {
                        if (Convert.ToInt64(reader["c_co_id"].ToString()) != ccoid)
                        {
                            result += (ccoid != 0 ? "]}," : "") + @"{""country"":""" + reader["c_co"].ToString() + @""",""cities"":[";
                            cccio = 0;
                        }
                        result += (cccio > 0 ? "," : "") + @"{""city"":{""id"":" + reader["c_id"].ToString() + @",""area"":""" + reader["c_p"].ToString() + @""",""name"":""" + reader["c_name"].ToString().Replace(@"""", "") + @"""}}";
                        ccoid = Convert.ToInt64(reader["c_co_id"].ToString());
                        cccio = cccio + 1;
                    }
                    result += @"]}";                    
                    result += @"]}";
                    result = result.Replace(@"\\", @"\\\\");                        
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Exception Error Message: " + ex.Message);
                }
                finally
                {
                    reader.Close();
                    reader.Dispose();
                    reader = null;
                    if (connection.State != System.Data.ConnectionState.Closed)
                        connection.Close();
                }

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

    }
}
