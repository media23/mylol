﻿using mylol.Helper;
using mylol.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Data
{
    public class ProfilesPhotosGalleryRepo
    {

        public static int AddProfilesPhotosGallery(ProfilesPhotosGallery profilesPhotosGallery)
        {
            int result = 0;

            using (SqlConnection connection = new SqlConnection(ConfigManager.GetConnectionString()))
            {
                string sql = @" INSERT INTO profiles_photos_gal (ppg_name,  ppg_pro_id, ppg_date_created, ppg_date_modified, ppg_cover_pic, ppg_nbr_pic, ppg_nbr_comment, ppg_nbr_like, ppg_type_id, ppg_isdelete ) 
                                                         VALUES (      '', @ppg_pro_id,        GETDATE(),         GETDATE(),            '',           0,               0,            0,           2,            1 ) ";

                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@ppg_pro_id", profilesPhotosGallery.ppg_pro_id);
                cmd.CommandType = CommandType.Text;

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                cmd.ExecuteNonQuery();

                if (connection.State != ConnectionState.Closed)
                    connection.Close();
            }

            return result;
        }

    }
}
