﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Model
{
    public class SiteCity
    {
        public long c_id { set; get; } // city id
        public string c_name { set; get; } // city name
        public string r_name { set; get; } // region name
    }
}
