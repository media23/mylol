﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Model
{
    public class Profile
    {
        public long pro_id { set; get; }
        public long pro_reg_id { set; get; }
        public long pro_regions { set; get; }
        public long pro_top5 { set; get; }
        public string pro_login { set; get; }
        public string pro_dob { set; get; }
        public long pro_age { set; get; }
        public long pro_sex { set; get; }
        public string pro_ville { set; get; }   // city name
        public long pro_c_id { set; get; }
        public string pro_realemail { set; get; }
        public string pro_pic_url { set; get; } // NULL
        public string pro_date { set; get; } // getdate()
        public long pro_show { set; get; } // 21
        public long pro_visit { set; get; } // 0
        public long pro_vo_show { set; get; } // 1
        public long pro_chat_show { set; get; } // 1
        public long pro_sendemail { set; get; } // 0
        public long pro_orisexe { set; get; } // 3
        public string pro_fb_id { set; get; }
        public string pro_twitter_id { set; get; }
        public long pro_mbr_bar_gender { set; get; }
        public long pro_level { set; get; }
        public long pro_co_id { set; get; }
        public long pro_p_id { set; get; }
        public long pro_web_id { set; get; }
        public long pro_statut { set; get; } // 1
        public string pro_ip { set; get; }
        public string pro_geo { set; get; }
        public long pro_latitude { set; get; }
        public long pro_longitude { set; get; }

        public string pro_pass { set; get; } // entered password
        public string pro_classeur { set; get; } // user message security

        /*
         * "INSERT INTO Profiles(
         * pro_reg_id, 
         * pro_regions,
         * pro_top5,
         * pro_login, 
         * pro_dob,
         * pro_age, 
         * pro_sex, 
         * pro_ville,
         * pro_c_id, 
         * pro_realemail, 
         * pro_pic_url, 
         * pro_date, 
         * pro_show, 
         * pro_visit, 
         * pro_vo_show,
         * pro_chat_show, 
         * pro_sendemail,
         * pro_orisexe,
         * pro_fb_id,
         * pro_twitter_id,
         * pro_mbr_bar_gender,
         * pro_level,
         * pro_co_id,
         * pro_p_id,
         * pro_web_id,
         * pro_statut,
         * pro_ip,
         * pro_geo,
         * pro_latitude,
         * pro_longitude) 
         * VALUES(" & pro_reg & "," & pro_reg & ",0,''" & pro_login & "'',''" & pro_dob & "''," & pro_age & "," & pro_sex & ",
         * ''" & pro_ville & "''," 
         * & pro_ville_id 
         * & ",''" & pro_realemail 
         * & "'',
         * NULL,
         * getdate(),
         * 21,
         * 0,
         * 1,
         * 1,
         * 0,3,
         * ''"& pro_fbid &"'',
         * ''"& pro_twitterid &"'',
         * "&bar_gender&",
         * "&newLevel&",
         * "&pro_co_id&",
         * "&pro_p_id&",
         * "&session("web_id")
         * &",1,
         * ''"&GLOBAL_IP
         * &"'',geography::Point(''" & geoLAT & "'',''" & geoLGT & "'',4326)," 
         * & geoLAT & "," 
         * & geoLGT & ")"
         */
    }
}
