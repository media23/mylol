﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Model
{
    public class ProfilesPhotosGallery
    {
        public string ppg_name { set; get; }
        public long ppg_pro_id { set; get; } // province id
        public DateTime ppg_date_created { set; get; } // now
        public DateTime ppg_date_modified { set; get; } // now
        public string ppg_cover_pic { set; get; }
        public long ppg_nbr_pic { set; get; } // 0
        public long ppg_nbr_comment { set; get; } // 0
        public long ppg_nbr_like { set; get; } // 0
        public long ppg_type_id { set; get; } // 2
        public long ppg_isdelete { set; get; } // 1
        /*
db_master.execute("INSERT INTO profiles_photos_gal (
                    ppg_name,
                    ppg_pro_id,
                    ppg_date_created,
                    ppg_date_modified,
                    ppg_cover_pic,
                    ppg_nbr_pic,
                    ppg_nbr_comment,
                    ppg_nbr_like,
                    ppg_type_id,ppg_isdelete) 
                    values (
                    '',"
                    &pro_id&",
                    '"& now &"',
                    '"& now &"',
                    '',
                    0,
                    0,
                    0,
                    2,
                    1)")
         */

    }
}
