﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Model
{
    public class SiteRegion
    {
        public long r_id { set; get; } // region id
        public string r_name { set; get; } // region name

        public long pro_co_id { set; get; } // country id, to insert in profile
        public long pro_p_id { set; get; } // province id, to insert in profile
        public long pro_real_reg_id { set; get; } // real region id id, to insert in profile
    }
}
