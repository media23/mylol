﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Model
{
    public class LostPassword
    {
        public string message { set; get; }
        public long pro_id { set; get; }
        public long ps_lang { set; get; }
        public string pro_login { set; get; }
    }
}
