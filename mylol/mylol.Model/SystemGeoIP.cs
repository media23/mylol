﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Model
{
    public class SystemGeoIP
    {
        public string latitude { set; get; }
        public string longitude { set; get; }

        public string city_name { set; get; } // city name
        public string co_name { set; get; } // country name
    }
}
