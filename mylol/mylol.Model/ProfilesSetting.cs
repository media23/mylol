﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mylol.Model
{
    public class ProfilesSetting
    {
        public long ps_credits { set; get; }
        public long ps_global_msg_id { set; get; }
        public long ps_level { set; get; }
        public DateTime ps_level_date { set; get; }
        public long ps_pro_id { set; get; }
        public long ps_lang { set; get; }
        public long ps_activated { set; get; } // 1
        public string ps_ip { set; get; }
        public long ps_timezone { set; get; } // (SELECT ISNULL(r_timezone,-5) from site_regions WITH(NOLOCK) where r_id = "&pro_reg&")
        public long ps_chat { set; get; } // 3
        public long ps_privacy { set; get; } // 1
        public long ps_msg_nbr { set; get; } // 0

        /*
         sqlShoutFilter
			IF  session("web_id")=0  THEN ' Twiig
				sqlShoutFilter = "18,99"
				sqlCredits = "50"
			ELSE ' MyLOL
				sqlShoutFilter = "12,20"
				sqlCredits = "50"
			END IF
         */
        public long ps_shout_fr_age { set; get; }
        public long ps_shout_to_age { set; get; }

        public long r_id { set; get; } // region id

        /*
db_master.execute("INSERT INTO profiles_settings (
ps_credits,
ps_global_msg_id,
ps_level,
ps_level_date,
ps_pro_id, 
ps_lang, 
ps_activated, 
ps_ip, 
ps_timezone, 
ps_chat,
ps_privacy, 
ps_msg_nbr,
ps_shout_fr_age,ps_shout_to_age) 
values ("
&sqlCredits&","
&session("ps_global_msg_id")&","
&newLevel&","
&newLevelDate&","
&pro_id&","
&us_lang&",
1,
'"&GLOBAL_IP&"', 
(SELECT ISNULL(r_timezone,-5) from site_regions WITH(NOLOCK) where r_id = "&pro_reg&"), 
3,
1,
0,
"&sqlShoutFilter&")")
         */
    }
}
